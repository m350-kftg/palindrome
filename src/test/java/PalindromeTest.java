import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PalindromeTest {

    @Test
    public void sugusIsAPalindrome(){
        // given - nothing to do
        // when
        boolean isPalindrome = Palindrome.isPalindrome("SUGUS");
        // then
        assertTrue(isPalindrome);
    }

    @Test
    public void mentosIsNotAPalindrome(){
        // given - nothing to do
        // when
        boolean isPalindrome = Palindrome.isPalindrome("MENTOS");
        // then
        assertFalse(isPalindrome);
    }

}
