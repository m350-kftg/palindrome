public class Palindrome {

    public static boolean isPalindrome(String toTest){
        int len = toTest.length();
        for (int i = 1; i < len/2; i++) {
            if (toTest.charAt(i) != toTest.charAt(len-i-1)){
                return false;
            }
        }
        return true;
    }
}